var mongoose = require('mongoose');
var TodoSchema = new mongoose.Schema({
    todoDescription: { type: String, required: true }
});
module.exports = mongoose.model('todos', TodoSchema);