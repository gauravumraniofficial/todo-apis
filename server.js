var express = require('express');
var bodyParser = require('body-parser');
var todoController = require('./controllers/todoController');
var mongoose = require('mongoose');
var app = express();


// Connect To Database
mongoose.connect('mongodb://localhost:27017/todos');

// On Connection
mongoose.connection.on('connected', function () {
    console.log('Connected to database todos');
});

// On Error
mongoose.connection.on('error', function (err) {
    console.log('Database error: ' + err);
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


// get all todo
app.get('/', todoController.getTodos);

// get single todo
app.get('/:todo_id', todoController.getSingleTodo);

//save a todo
app.post('/', todoController.saveTodo);

// edit a todo
app.patch('/', todoController.editTodo);

// delete a todo
app.delete('/:todo_id', todoController.deleteTodo);


var port = process.env.PORT || 3000;
app.listen(port, function () {
    console.log('Server running on ' + port);
});