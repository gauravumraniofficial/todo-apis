var TodoModel = require('../models/Todos');
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
//method to get all todos
module.exports.getTodos = function (req, res) {
    TodoModel.find({}, function (err, data) {
        if (err) {
            console.log(err);
            return res.json({ success: false, msg: 'Technical problem, try later' });
        }
        if (!data.length) return res.json({ success: false, msg: 'No data found' });
        return res.json({ success: true, data: data });
    });
};

//method to get single todos
module.exports.getSingleTodo = function (req, res) {
    if (!req.params.todo_id) return res.json({ success: false, msg: 'Invalid todo' });
    var searchQuery = {
        "_id": req.params.todo_id
    }
    TodoModel.findOne(searchQuery, function (err, data) {
        if (err) {
            console.log(err);
            return res.json({ success: false, msg: 'Technical problem, try later' });
        }
        if (!data) return res.json({ success: false, msg: 'No data found' });
        return res.json({ success: true, data: data });
    });
};


//method to save a todo
module.exports.saveTodo = function (req, res) {
    if (!req.body.todo || req.body.todo.trim() == "") return res.json({ success: false, msg: 'Please enter a todo name' });
    var todo = new TodoModel({
        todoDescription: req.body.todo
    });
    todo.save(function (err, data) {
        if (err) {
            console.log(err);
            return res.json({ success: false, msg: 'Error in saving, try again later' });
        }
        return res.json({ success: true, msg: 'Todo successfully added' });
    });
};

//method to edit a todo
module.exports.editTodo = function (req, res) {
    if (!req.body.todo_id) return res.json({ success: false, msg: 'Invalid todo' });
    if (!req.body.todo || req.body.todo.trim() == "") return res.json({ success: false, msg: 'Please enter a todo name' });
    var todo_id = req.body.todo_id;
    var todo = req.body.todo;
    var searchQuery = {
        "_id": ObjectId(todo_id)
    }
    var updateQuery = {
        "todoDescription": todo
    }
    TodoModel.update(searchQuery, updateQuery, function (err, data) {
        if (err) {
            console.log(err);
            return res.json({ success: false, msg: 'Error in updating todo, try again later' });
        }
        if (data.nModified && data.n) return res.json({ success: true, msg: 'Todo successfully updated' });
        return res.json({ success: true, msg: 'Cant updated todo' });
    });
};

//method to delete a todo
module.exports.deleteTodo = function (req, res) {
    if (!req.params.todo_id) return res.json({ success: false, msg: 'Invalid todo' });
    var todo_id = req.params.todo_id;
    var deleteQuery = {
        "_id": ObjectId(todo_id)
    }
    TodoModel.remove(deleteQuery, function (err, data) {
        if (err) {
            console.log(err);
            return res.json({ success: false, msg: 'Error in updating todo, try again later' });
        }
        if (data.n) return res.json({ success: true, msg: 'Todo successfully deleted' });
        return res.json({ success: true, msg: 'Cant delete todo' });
    });
};